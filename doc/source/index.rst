.. compass-specs documentation master file

==============================
Compass Project Specifications
==============================

Contents:

.. toctree::
   :glob:
   :maxdepth: 1

   specs/*

2.0 approved specs:

.. toctree::
   :glob:
   :maxdepth: 1

   specs/2.0/*

==================
Indices and tables
==================

* :ref:`search`
